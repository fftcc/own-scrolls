# 2.1.0-rc (23 jul 2022)
- minor update: add module ProgressBar

# 2.0.1-rc (10 jul 2022)
- fixup alignToTop paremeter in ScrollToSection module 

# 2.0.0-rc (10 jul 2022)
- major update: ScrollToSection module: switching from scrollTo to a more flexible scrollIntoView

# 1.0.2-rc (05 jul 2022)
- Minified NPM package

# 1.0.0-rc (03 jul 2022)
- Add scroll to section
- Add scrollbar
- Add scroll by pixel