export default class ScrollBy {
  constructor(props) {
    this.x = props.x;
    this.y = props.y;
    this.button = document.querySelector(props.button);
  }

  setEventListeners() {
    this.button.addEventListener('click', () => {
      this.scroll();
    });
  }

  scroll() {
    window.scrollBy({
      left: this.x,
      top: this.y,
      behavior: 'smooth',
    });
  }
}
