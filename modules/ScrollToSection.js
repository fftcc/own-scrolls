export default class ScrollToSection {
  constructor(props) {
    this.anchorLink = document.querySelector(props.anchorLink);
    this.anchorSection = document.querySelector(props.anchorSection);
    this.horizontal = props.horizontal;
    this.vertical = props.vertical;
    this.behavior = props.behavior;
    this.alignToTop = props.alignToTop;
  }

  setEventListeners() {
    this.anchorLink.addEventListener('click', () => {
      this._isScroll(this.anchorSection);
    });
  }

  _isScroll(anchorSection) {
    if (this.alignToTop === false) {
      anchorSection.scrollIntoView({
        block: 'end',
        inline: 'nearest',
        behavior: this.behavior,
      });
    } else {
      anchorSection.scrollIntoView({
        block: this.horizontal,
        inline: this.vertical,
        behavior: this.behavior,
      });
    }
  }
}
