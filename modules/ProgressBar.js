import './ProgressBar.css';

export default class ProgressBar {
  constructor() {
    document.body.insertAdjacentHTML(
      'afterbegin',
      '<div class="progress progress__container"><div class="progress progress__indicator"></div></div>'
    );
    this.progressBar = document.querySelector('.progress__indicator');
  }

  _fillProgressLine() {
    this.progressBar.style.width = `${
      (scrollY * 100) / (document.body.scrollHeight - window.innerHeight)
    }%`;
  }

  setEventListeners() {
    window.addEventListener('scroll', () => {
      this._fillProgressLine();
    });
    window.addEventListener('resize', () => {
      this._fillProgressLine();
    });
  }
}
