import './ScrollBar.css';

export default class ScrollBar {
  constructor() {
    document.body.insertAdjacentHTML(
      'afterbegin',
      '<div class="scroll scroll__container"><div class="scroll scroll__indicator"></div></div>'
    );
    this.html = document.querySelector('html');
    this.scrollBar = document.querySelector('.scroll__container');
    this.scrollBarIndicator = document.querySelector('.scroll__indicator');
    this.isClicked = false;
    this.timer = null;
    this.scrollBarIndicator.style.height = `${
      (this.scrollBar.clientHeight * document.documentElement.clientHeight) /
      document.documentElement.scrollHeight
    }px`;
  }

  _indicate() {
    this.scrollBarIndicator.style.height = `${
      (this.scrollBar.clientHeight * document.documentElement.clientHeight) /
      document.documentElement.scrollHeight
    }px`;
    this.scrollBarIndicator.style.top = `${
      (this.scrollBar.clientHeight * document.documentElement.scrollTop) /
      document.documentElement.scrollHeight
    }px`;
  }

  _scrollToPoint(e) {
    const clickedPoint = e.clientY / this.scrollBar.offsetHeight;
    scrollTo(
      0,
      clickedPoint * document.documentElement.scrollHeight -
        document.documentElement.clientHeight
    );
  }

  _setHideSlide() {
    this.scrollBar.classList.add('hide__slide');
    this.scrollBar.classList.remove('visible__slide');
  }

  _setVisibleSlide() {
    this.scrollBar.classList.remove('hide__slide');
    this.scrollBar.classList.add('visible__slide');
  }

  _setHideZoom() {
    this.scrollBar.classList.add('hide__zoom');
    this.scrollBar.classList.remove('visible__zoom');
  }

  _setVisibleZoom() {
    this.scrollBar.classList.remove('hide__zoom');
    this.scrollBar.classList.add('visible__zoom');
  }

  setEventListeners() {
    window.addEventListener('scroll', () => {
      this._indicate();
    });

    this.scrollBar.addEventListener('click', (e) => {
      this.html.classList.remove('active');
      this._scrollToPoint(e);
    });

    this.scrollBar.addEventListener('mousedown', (e) => {
      this.isClicked = true;
      this._scrollToPoint(e);
    });

    window.addEventListener('mouseup', () => {
      this.isClicked = false;
      this.html.classList.remove('active');
    });

    this.html.addEventListener('mousemove', (e) => {
      if (this.isClicked) {
        this.html.classList.add('active');
        this._scrollToPoint(e);
      }
    });
  }
}

