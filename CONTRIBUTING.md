# Contributing to `own-scrolls`

Thanks for contributing to `own-scrolls`!

## Submitting an issue

### 1. Search for Duplicates

Before you submit an issue, please search the issue tracker as it may already exist or even have been fixed.

### 2. Use a clear and descriptive title

A good title may catch our attention and therefore, your issue may be resolved quickly.

### 3. Include as much information as possible

If you are logging a bug, make sure to include the following:

- The version of `own-scrolls`
- The browser you are running on
- Steps to reproduce the behavior

### 4. Provide an online demo if possible

An online demo is a lifesaver! You can create an online demo on [codepen](https://codepen.io/)

### 5. Be patient

We want to fix all the issues as soon as possible, but we can't make guarantees about how fast your issue can be resolved. Your understanding and patience is greatly appreciated.

## Submitting a pull request

### 1. Make your changes in a new git branch

```
$ git checkout -b my-fix-branch develop
```

### 2. Follow the code style

Run `npm run lint` before committing.

### 4. Don't include unrelated changes

DO NOT include `dist/*` in your commit. Bundle files will be updated when publishing new version.

### 5. Don't submit PRs against the `main` branch

The `main` branch is considered as a snapshot of the latest release. All development should be done in the `develop` branch.

### 6. Use a clear and descriptive title for your PR

### 7. Write a convincing description

- If you are fixing a bug:

    - Provide detailed description of the bug, or links to the related issues.

- If you are adding new features:

    - Provide convincing reason to add this reason.


## Development setup

Before starting, make sure you are using [Node.js](http://nodejs.org/) 6+.

After cloning the repo, run:

```bash
$ npm install
```

Then run:

```bash
$ npm start
```
