<img align="center" src="https://codeberg.org/fftcc/assets/raw/branch/main/icons/logo22.png">


[![npm](https://img.shields.io/npm/v/own-scrolls?color=%239580ff&style=for-the-badge)](https://www.npmjs.com/package/own-scrolls) ![NPM](https://img.shields.io/npm/l/own-scrolls?color=%23ff99cc&style=for-the-badge) ![npm bundle size](https://img.shields.io/bundlephobia/min/own-scrolls?color=%235374fa&style=for-the-badge)
![npm](https://img.shields.io/npm/dm/own-scrolls?color=%2350ee80&style=for-the-badge)

**Custom scrollbar, Progressbar, Smooth scroll to section, Scroll by pixels**

## [DEMO WEBSITE](https://own-scrolls.ff99cc.art)

**Install**

```sh
npm i own-scrolls --save
```

## Documentation

- [Scroll to section](#scroll-to-section)
  - [Parameter `alignToTop`](#parameter-alignToTop`)
  - [Parameter `behavior`](#parameter-behavior)
  - [Parameter `horizontal`](#parameter-horizontal)
  - [Parameter `vertical`](#parameter-vertical)
- [Scroll by pixels](#scroll-by-pixels)
- [Custom Scrollbar](#custom-scrollbar)
- [Progressbar](#progressbar)

### Scroll to section

> ⚠️ ⚠️ ⚠️ In version 2.0.0-rc, the `anchorButton` parameter was replaced with `anchorLink`

> ⚠️ Some non-popular browsers do not support behavior `smooth`

usage example:

```js
// Import ScrollToSection module
import { ScrollToSection } from 'own-scrolls';

// Create an instance
// Pass to the "anchorButton" parameter the selector of the class or tag of the button (anchor)
// Pass to the "anchorSection" parameter the selector of the class or tag of the section to which you need to move by clicking on the button
const instanceOne = new ScrollToSection({
  anchorLink: '#button1',
  anchorSection: '#section1',
  horizontal: 'end',
  behavior: 'smooth',
});

// Call the scrollToSectionInstanceOne constant with the setEventListeners method
instanceOne.setEventListeners();

const instanceTwo = new ScrollToSection({
  anchorLink: '#button2',
  anchorSection: '#section2',
  horizontal: 'center',
  vertical: 'center',
  behavior: 'auto',
});
instanceTwo.setEventListeners();

const instanceThree = new ScrollToSection({
  anchorLink: '.button',
  anchorSection: '.section',
  alignToTop: true,
});
instanceThree.setEventListeners();
```

#### Parameter `alignToTop`

(Can set `behavior`)

Takes the values:

`true`, the upper border of the element will be aligned with the top of the visible scroll area. Match:

```
{
  horizontal: "start",
  vertical: "nearest"
}
```

`false`, the lower border of the element will be aligned to the bottom of the visible scroll area. Match:

```
{
  horizontal: "end",
  vertical: "nearest"
}
```

#### Parameter `behavior`

Scrolling animation. Takes the values `"auto"` or `"smooth"`.

#### Parameter `horizontal`

Horizontal alignment.
One of the values: `"start"`, `"center"`, `"end"` or `"nearest"`.

#### Parameter `vertical`

Vertical alignment.
One of the values: `"start"`, `"center"`, `"end"` or `"nearest"`.

<br>

### Scroll by pixels

usage example:

```js
// Import ScrollBy module
import { ScrollBy } from 'own-scrolls';

// Create an instance
// Pass to the "y" parameter a number that is the number of pixels
// Pass to the "button" parameter the selector of the class or tag on clicking on which scrolling will occur
const instanceDown = new ScrollBy({
  y: -100,
  x: 0, // If the parameter is zero, it is not necessary to pass it
  button: '.button-down'
})
// Call the instanceDown constant with the setEventListeners method
instanceDown.setEventListeners();

const instanceUp = new ScrollBy({
  y: 100,
  button: '.button-up'
})
instanceUp.setEventListeners();

// Similarly, the left\right scroll is set by changing the value of "x"
```
<br>

### Custom Scrollbar

usage example:

```js
// Import ScrollBar module
import { ScrollBar } from 'own-scrolls';

// Create an instance
const scrollbar = new ScrollBar();

// Call the scrollbar constant with the setEventListeners method
scrollbar.setEventListeners();

```

**Styles:**

- It is necessary to import the `ScrollBar` module in the entry file JS, before importing your own styles, since the `ScrollBar` module contains default styles. This is necessary to be able to overwrite the default styles with your own.

example:

index.js
```js
import { ScrollBar } from 'own-scrolls';
import './index.css';

...
/* some js code */
```

- Next, you need to write selectors in your own CSS file: `.scroll__container` and `.scroll__indicator` and set styles at your discretion.

**Default styles:**

Some properties contain variables, the values of variables are set in root.
```css
...
/* some css code */
...

.scroll__container {
  background: rgba(90, 90, 90, 0.2);
  transition: var(--smooth-scroll);
}

.scroll__indicator {
  height: 0;
  width: var(--width-scroll-indicator);
  border-radius: 100vh;
  background: linear-gradient(to top, red, blue);
  transition: var(--smooth-scroll);
}
```

If there is a need to change the value of a variable, you need to write the selector `:root` in your own main CSS file (it is recommended to write at the very beginning of the file) and set the values of the necessary variables inside.

**Default values of variables:**

```css
:root {
  --width-scroll-container: 0.5vw;
  --width-scroll-indicator: 0.5vw;
  --smooth-scroll: 0.5s;
}

...
/* some css code */
```

<br>

### Progressbar

usage example:

```js
// Import ProgressBar module
import { ProgressBar } from 'own-scrolls';

// Create an instance
const progressbar = new ProgressBar();

// Call the progressbar constant with the setEventListeners method
progressbar.setEventListeners();

```

**Styles:**

- It is necessary to import the `ProgressBar` module in the entry file JS, before importing your own styles, since the `ProgressBar` module contains default styles. This is necessary to be able to overwrite the default styles with your own.

example:

index.js
```js
import { ProgressBar } from 'own-scrolls';
import './index.css';

...
/* some js code */
```

- Next, you need to write selectors in your own CSS file: `.progress__container` and `.progress__indicator` and set styles at your discretion.

**Default styles:**

Some properties contain variables, the values of variables are set in root.
```css
...
/* some css code */
...

.progress__container {
  height: var(--height-progress-container);
  background: rgba(90, 90, 90, 0.2);
}
.progress__indicator {
  height: var(--height-progress-indicator);
  width: 0;
  background: linear-gradient(90deg, red, blue);
}

```

If there is a need to change the value of a variable, you need to write the selector `:root` in your own main CSS file (it is recommended to write at the very beginning of the file) and set the values of the necessary variables inside.

**Default values of variables:**

```css
:root {
  --height-progress-container: 1vh;
  --height-progress-indicator: 1vh;
}

...
/* some css code */
```

<br>

#### Buy an avid coffee lover a cup of coffee !

[![](https://codeberg.org/fftcc/Buy-me-a-coffee/raw/main/btc/btc.png)](https://codeberg.org/fftcc/Buy-me-a-coffee/src/main/btc/btc.md)

[![](https://codeberg.org/fftcc/Buy-me-a-coffee/raw/main/trx/trx-usdt.png)](https://codeberg.org/fftcc/Buy-me-a-coffee/src/main/trx/trx.md)

[![](https://codeberg.org/fftcc/Buy-me-a-coffee/raw/main/xmr/xmr.png)](https://codeberg.org/fftcc/Buy-me-a-coffee/src/main/xmr/xmr.md)

[![](https://codeberg.org/fftcc/Buy-me-a-coffee/raw/main/eth/eth.png)](https://codeberg.org/fftcc/Buy-me-a-coffee/src/main/eth/eth.md)

[**More coin...**](https://codeberg.org/fftcc/Buy-me-a-coffee/src/main/Buy-me-a-coffee.md)


<br>

Made with <img src="https://codeberg.org/fftcc/Buy-me-a-coffee/raw/main/badges/coffee3.png" /> by fftcc.
